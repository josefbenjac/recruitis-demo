ssh $SSH_HOST docker-compose -f $PROJECT_FOLDER/docker-compose.yml down --remove-orphans &&
ssh $SSH_HOST docker-compose -f $PROJECT_FOLDER/docker-compose.yml up -d &&
ssh $SSH_HOST docker-compose -f $PROJECT_FOLDER/docker-compose.yml exec -T php bin/console cache:clear --ansi
