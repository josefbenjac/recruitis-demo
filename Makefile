# Start dev environment
up:
	@echo "UID=`id -u`" > .docker/local/.env
	@echo "GID=`id -g`" >> .docker/local/.env
	@[[ -f .docker/local/data/.bash_history ]] || touch .docker/local/data/.bash_history
	@docker-compose -p recruitis-demo -f .docker/local/docker-compose.yml up -d --remove-orphans
	@echo 'App is running on http://localhost'

# Start dev environment with forced build
up\:build:
	@echo "UID=`id -u`" > .docker/local/.env
	@echo "GID=`id -g`" >> .docker/local/.env
	@[[ -f .docker/local/data/.bash_history ]] || touch .docker/local/data/.bash_history
	@docker-compose -p recruitis-demo -f .docker/local/docker-compose.yml up -d --build
	@echo 'App is running on http://localhost'

# Stop dev environment
down:
	@docker-compose -p recruitis-demo -f .docker/local/docker-compose.yml down

# Show logs - format it using less
logs:
	@docker-compose -p recruitis-demo  -f .docker/local/docker-compose.yml logs -f --tail=10 | less -S +F

# Exec bash on PHP container
exec\:php:
	@docker-compose -p recruitis-demo -f .docker/local/docker-compose.yml exec php bash

# Prepare project
setup:
	@docker-compose -p recruitis-demo -f .docker/local/docker-compose.yml exec php composer install
	@docker-compose -p recruitis-demo -f .docker/local/docker-compose.yml exec php php bin/console cache:clear

# Rebuild cache
clean:
	@docker-compose -p recruitis-demo -f .docker/local/docker-compose.yml exec php php bin/console cache:clear
